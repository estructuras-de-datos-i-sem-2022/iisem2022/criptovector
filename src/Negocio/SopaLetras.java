/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Modelo.Cadena;

/**
 * Está clase simula una matriz de char, donde se tiene un vector de cadenas que
 * a su vez contienen otras cadenas.
 * 
 *
 * @author madarme
 */
public class SopaLetras {

    private Cadena[] cadenas;

    public SopaLetras() {
    }

    public SopaLetras(String[] palabras) {
        if (palabras == null) {
            throw new RuntimeException("No es posible crear la sopa porque no hay palabras");
        }
        this.cadenas = new Cadena[palabras.length];
        for (int i = 0; i < this.cadenas.length; i++) {
            this.cadenas[i] = new Cadena(palabras[i]);
        }
    }

    public String toString() {
        String msg = "";
        for (Cadena c : this.cadenas) {
            msg += c.toString() + "\n";
        }
        return msg;
    }

    public char get(int i, int j) {

        /*
            Lo dummy:
        
        Cadena aux = this.getFila(i, j);
        
        return aux.get(j);
         */
        // :)
        return this.getFila(i, j).get(j);
    }

    public void set(int i, int j, char nuevaLetra) {

        this.getFila(i, j).set(j, nuevaLetra);
    }

    private Cadena getFila(int i, int j) {
        this.validarFila(i);
        Cadena aux = this.cadenas[i];
        this.validarColumna(aux, j);
        return aux;

    }

    private void validarColumna(Cadena aux, int j) {
        if (j < 0 || j >= aux.length()) {
            throw new RuntimeException("Inválido índice para las columnas");
        }
    }

    private void validarFila(int i) {
        if (i < 0 || i >= this.cadenas.length) {
            throw new RuntimeException("Inválido índice para las filas");
        }
    }

    /**
     * Método que retorna la cantidad de filas de la sopa
     *
     * @return un entero con la cantidad de filas
     */
    public int length() {
        return this.cadenas.length;
    }

    //solo deben usar get(i,j)
    public int getCantidadVeces_DiagonalSup(Cadena c) {
        /**
         * :)
         */
        return -1;
    }

    public int getCantidadVeces_DiagonalInf(Cadena c) {
        /**
         * :)
         */
        return -1;
    }

    public int getCantidadVeces_Hor(Cadena c) {
        /**
         * :)
         */
        return -1;
    }

    public int getCantidadVeces_Ver(Cadena c) {
        /**
         * :) :)
         */
        return -1;
    }

    public int getCoindicencias(Cadena c) {
        return this.getCantidadVeces_DiagonalInf(c) + this.getCantidadVeces_DiagonalSup(c) + this.getCantidadVeces_Hor(c) + this.getCantidadVeces_Ver(c);
    }
    
}
