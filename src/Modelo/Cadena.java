/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author madarme
 */
public class Cadena {

    private char letras[];

    public Cadena() {
    }

    public Cadena(String palabra) {

        if (palabra == null || palabra.isEmpty()) {
            throw new RuntimeException("Imposible crear la cadena está vacía");
        }

        //Por hoy hacemos acto de fé que introduzco bien los datos hasta el viernes qe veamos excepciones
        this.letras = new char[palabra.length()];
        for (int i = 0; i < this.letras.length; i++) {
            this.letras[i] = palabra.charAt(i);
        }
    }

    public char[] getLetras() {
        return letras;
    }

    @Override
    public String toString() {

        String msg = "";
        for (char dato : this.letras) {
            msg += dato + "\t";
        }
        return msg;
    }

    /**
     * HAGO ACTO DE FE QUE INTRODUZCO BIEN LOS DATOS HASTA EL VIERNES
     *
     * @param salto dato entero para encriptar cada letra
     * @return un objeto de la clase Cadena con la palabra encriptada
     */
    public Cadena getEncriptar(byte salto) {

        if (salto <= 0) {
            throw new RuntimeException("Valor del salto no permitido:" + salto);
        }

        Cadena aux = new Cadena();
        aux.letras = new char[this.letras.length];
        for (int i = 0; i < aux.letras.length; i++) {
            aux.letras[i] = (char) ((byte) (this.letras[i]) + salto);
        }
        return aux;
    }

    public int length() {
        return this.letras.length;
    }

    public char get(int i) {
        this.validarIndice(i);
        return this.letras[i];

    }

    public void set(int i, char nuevaLetra) {

        this.validarIndice(i);
        this.letras[i] = nuevaLetra;

    }

    private void validarIndice(int i) {
        if (i < 0 || i >= this.letras.length) {
            throw new RuntimeException("Valor de índice en cadena inválido");
        }

    }

}
