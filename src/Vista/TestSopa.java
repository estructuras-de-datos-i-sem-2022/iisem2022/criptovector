/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Cadena;
import Negocio.SopaLetras;

/**
 *
 * @author madarme
 */
public class TestSopa {

    public static void main(String[] args) {
        String palabras[] = {"MADARME", "EILEN", "PILAR", "CRISTINA", "ANA"};

        try {
            SopaLetras s = new SopaLetras(palabras);
            System.out.println("Mi matriz es:\n" + s.toString());
            System.out.println("\n(0,2)="+s.get(0, 2));
            
            System.out.println("MADARME está"+s.getCoindicencias(new Cadena("MADARME")));
            
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }
}
