/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Cadena;

/**
 *
 * @author madarme
 */
public class TestCripto {

    public static void main(String[] args) {

        String dato = "MADARME";
        try {
            Cadena c = new Cadena(dato);
            System.out.println(c);
            Cadena enc = c.getEncriptar((byte) -2);
            System.out.println(enc);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

}
